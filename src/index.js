const express = require('express');
const morgan = require('morgan');

const allowCrossDomain = require('./skillBranchCORS');
const canonize = require('./canonize');

const app = express();

morgan.token('decodedURL', req => decodeURIComponent(req.url));

app.use(allowCrossDomain);
app.use(morgan(':method :decodedURL :status :res[content-length] - :response-time ms'));

app.get('/', (req, res) => {
  const input = req.query.username;
  try {
    const userName = canonize(input);
    res.send(userName);
  } catch (e) {
    res.send('error');
    console.log(e);
  }
});

app.listen(3000, () => console.log('Server runing on 3000 port!'));
