const URL = require('url');

const isValidUserName = str => /^@*[\w.]{3,}$/i.test(str.trim());
const format = (userName) => {
  const clrUserName = userName.trim().replace(/^@*/, '');
  return `@${clrUserName}`;
};

module.exports = (str) => {
  if (isValidUserName(str)) {
    return format(str);
  }

  let normUrl = decodeURIComponent(str).trim().replace(/^\/{1,2}/, 'http://');
  if (!/^https?:\/\//i.test(normUrl)) normUrl = `http://${normUrl}`;

  const path = URL.parse(normUrl).path.replace(/^[/@#$%&*!-+|?.,]+/, '');
  const possibleUserName = path.split('/')[0];

  if (!possibleUserName) throw new Error('Invalid str');

  if (!isValidUserName(possibleUserName)) {
    throw new Error('Invalid str');
  }

  const userName = possibleUserName.toLowerCase();
  return format(userName);
};
